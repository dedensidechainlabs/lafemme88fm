<?php $this->load->view('vheader'); ?>

<section class="about">
		<div class="container">
			<div class="row">
				<div class="col-md-9 animated wow fadeInLeft">
					<div class="panel">
						<?php foreach($getMovieDetail as $getMovieDetail): ?>
						<div class="head-panel">
							<h4><a href="<?php echo base_url(); ?>movie/">MOVIE</a> &#8658; <?php echo $getMovieDetail->contenttitle; ?></h4>
						</div>
						<div class="body-panel">
							<div class="row">
								<div class="col-lg-12">
									<div class="img-content">
										<img class="img-thumbnail" src="<?php echo base_url();?>media/<?php echo $getMovieDetail->contentimages; ?>" alt="<?php echo $getMovieDetail->contenttitle; ?>" title="<?php echo $getMovieDetail->contenttitle; ?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-first-content">
										<h2><strong><?php echo $getMovieDetail->contenttitle; ?></strong></h2>
										<p class="text-date"><?php echo date("D, d M Y H:i:s",strtotime($getMovieDetail->contentcreatedate)); ?></p>
										<p><?php echo $getMovieDetail->contentfull; ?></p>
									</div>
								</div>
							</div>

							<div class="separator"></div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-md-3 animated wow fadeInRight">
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner-300x250.jpg" alt="" title="">	
					</div>	
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner2-300x250.jpg" alt="" title="">	
					</div>	
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner-300x250.jpg" alt="" title="">	
					</div>			
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner2-300x250.jpg" alt="" title="">	
					</div>	
				</div>
			</div>
		</div>

	</section>

<?php $this->load->view('vfooter'); ?>