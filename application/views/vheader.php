<!DOCTYPE html>
<html>
<head>
	<!--  App Title  -->
	<title><?php echo (isset($title)?$title:"")?></title>
	<!--  App Description  -->
	<meta name="description" content='<?php echo (isset($description)?$description:"")?>'/>
	<meta charset="utf-8">
	<meta name="author" content="Sidechain Labs">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.transitions.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.carousel.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/main.css"/>

	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ajaxchimp.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/scrollTo.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/wow.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parallax.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/nicescroll.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>

    <!-- flowplayer javascript component -->
    <script src="http://releases.flowplayer.org/js/flowplayer-3.2.13.min.js"></script>
</head>
<body>
	
	<!--  Header Section  -->
	<header>
		<div class="container-full">
			
			<div class="header-top text-center">
				<a href="https://www.facebook.com/pages/Radio-LaFemme-88FM/210024759035216" target="_blank"><img src="<?php echo base_url();?>assets/img/facebook.png" width="30"></a>&nbsp;
				<a href="https://twitter.com/newlafemmefm" target="_blank"><img src="<?php echo base_url();?>assets/img/twitter.png" width="30"></a>&nbsp;
				<a href="#" target="_blank"><img src="<?php echo base_url();?>assets/img/instagram.png" width="30"></a>&nbsp;
				<a href="https://www.youtube.com/channel/UCIuZYn11OSGXzQBXTTNEHrA" target="_blank"><img src="<?php echo base_url();?>assets/img/youtube.png" width="30"></a>&nbsp;


				<span>	
					<a href="<?php echo base_url();?>streaming" target="_blank">
						LIVE STREAMING 
						<img src="<?php echo base_url();?>assets/img/play-button.png" width="20">
						

					</a>
				</span>
			</div>
		</div>

		<div class="container">
			<div class="logo pull-left animated wow fadeInLeft">
				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/logo.png" alt="" title=""></a>
			</div>

			
			<nav class="pull-right">
				<ul class="list-unstyled">
					<li class="animated wow fadeInLeft" data-wow-delay="0s"><a href="<?php echo base_url();?>">HOME</a></li>
					<li class="animated wow fadeInLeft" data-wow-delay="0s"><a href="<?php echo base_url();?>music">MUSIC</a></li>
					<li class="animated wow fadeInLeft" data-wow-delay=".1s"><a href="<?php echo base_url();?>movie">MOVIE</a></li>
					<li class="animated wow fadeInLeft" data-wow-delay=".2s"><a href="<?php echo base_url();?>program">PROGRAM</a></li>
					<li class="animated wow fadeInLeft" data-wow-delay=".2s"><a href="<?php echo base_url();?>news">NEWS</a></li>
					<li class="animated wow fadeInLeft" data-wow-delay=".2s"><a href="<?php echo base_url();?>djs">DJs</a></li>
					<li class="animated wow fadeInLeft" data-wow-delay=".2s"><a href="<?php echo base_url();?>event">EVENT</a></li>
				</ul>
			</nav>

			<!-- <div class="social pull-right">
				<ul class="list-unstyled">
					<li class="animated wow fadeInRight" data-wow-delay=".2s"><a href="#"><img src="<?php echo base_url(); ?>assets/img/facebook.png" alt="" title=""></a></li>
					<li class="animated wow fadeInRight" data-wow-delay=".1s"><a href="#"><img src="<?php echo base_url(); ?>assets/img/twitter.png" alt="" title=""></a></li>
					<li class="animated wow fadeInRight" data-wow-delay="0s"><a href="#"><img src="<?php echo base_url(); ?>assets/img/google.png" alt="" title=""></a></li>
				</ul>
			</div>

			<span class="burger_icon">menu</span> -->
		</div>
	</header>
	<!--  End Header Section  -->