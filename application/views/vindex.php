<?php $this->load->view('vheader'); ?>

	<section class="hero" id="hero">
		<div class="container">
		</div>
	</section>

	<section class="about">
		<div class="container">
			<div class="row">
				<div class="col-md-9 animated wow fadeInLeft">
					<div class="panel">
						<div class="head-panel">
							<h4>NEWS <span><a href="<?php echo base_url();?>news/">>> VIEW ALL</a></span></h4>
						</div>
						<div class="body-panel">
							<?php foreach($getNewsHomeLatest as $getNewsHomeLatest): ?>
							<div class="img-first-content">
								<img src="<?php echo base_url();?>media/<?php echo $getNewsHomeLatest->contentimages; ?>" alt="<?php echo $getNewsHomeLatest->contenttitle; ?>" title="<?php echo $getNewsHomeLatest->contenttitle; ?>">
							</div>
							<div class="text-first-content">
								<h3><?php echo $getNewsHomeLatest->contenttitle; ?></h3>
								<p class="text-date"><?php echo $getNewsHomeLatest->contentcreatedate; ?></p>
								<p><?php echo $getNewsHomeLatest->contentheader; ?></p>
								<a href="<?php echo base_url();?>news/<?php echo $getNewsHomeLatest->contentslug; ?>/"><button class="btn-danger">Read More</button></a>
							</div>
							<?php endforeach; ?>

							<div class="separator"></div>
							
							<div class="list-news">
								<div class="row">
									<?php foreach($getNewsHome as $getNewsHome): ?>
									<div class="col-lg-4 text-center">
										<img class="img-content"src="<?php echo base_url();?>media/<?php echo $getNewsHome->contentimages; ?>" alt="<?php echo $getNewsHome->contenttitle; ?>" title="<?php echo $getNewsHome->contenttitle; ?>">
										<h4><?php echo $getNewsHome->contenttitle; ?></h4>
										<a href="<?php echo base_url();?>news/<?php echo $getNewsHome->contentslug; ?>/"><button class="btn-danger">Read More</button></a>
									</div>
									<?php endforeach; ?>
								</div>
							</div>

							<div class="separator"></div>

						</div>

						<div class="head-panel">
							<h4>EVENT <span><a href="<?php echo base_url();?>event/">>> VIEW ALL</a></span></h4>
						</div>
						<div class="body-panel">
							
							<div class="list-news">
								<div class="row">
									<?php foreach($getEventHome as $getEventHome): ?>
									<div class="col-lg-4 text-center">
										<img class="img-content"src="<?php echo base_url();?>media/<?php echo $getEventHome->contentimages; ?>" alt="<?php echo $getEventHome->contenttitle; ?>" title="<?php echo $getEventHome->contenttitle; ?>">
										<h4><?php echo $getEventHome->contenttitle; ?></h4>
										<a href="<?php echo base_url();?>event/<?php echo $getEventHome->contentslug; ?>/"><button class="btn-danger">Read More</button></a>
									</div>
									<?php endforeach; ?>
								</div>
							</div>

							<div class="separator"></div>
							
						</div>
					</div>
				</div>
				<div class="col-md-3 animated wow fadeInRight">
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner-300x250.jpg" alt="" title="">	
					</div>	
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner2-300x250.jpg" alt="" title="">	
					</div>	
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner-300x250.jpg" alt="" title="">	
					</div>			
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner2-300x250.jpg" alt="" title="">	
					</div>	
				</div>
			</div>
		</div>

	</section>
	
	<section class="container-separator"></section>
	
	<section class="about" id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-9 animated wow fadeInLeft">
					<div class="panel">
						<div class="head-panel">
							<h4>MOVIE MOVE <span><a href="<?php echo base_url(); ?>movie/">>> VIEW ALL</a></span></h4>
						</div>
						<div class="body-panel">
							<?php foreach($getMovieHomeLatest as $getMovieHomeLatest): ?>
							<div class="img-first-content">
								<img src="<?php echo base_url();?>media/<?php echo $getMovieHomeLatest->contentimages; ?>" alt="<?php echo $getMovieHomeLatest->contenttitle; ?>" title="<?php echo $getMovieHomeLatest->contenttitle; ?>">
							</div>
							<div class="text-first-content">
								<h3><?php echo $getMovieHomeLatest->contenttitle; ?></h3>
								<p class="text-date"><?php echo $getMovieHomeLatest->contentcreatedate; ?></p>
								<p><?php echo $getMovieHomeLatest->contentheader; ?></p>
								<a href="<?php echo base_url(); ?>movie/<?php echo $getMovieHomeLatest->contentslug; ?>/"><button class="btn-danger">Read More</button></a>
							</div>
							<?php endforeach; ?>

							<div class="separator"></div>
							
							<div class="list-news">
								<div class="row">
									<?php foreach($getMovieHome as $getMovieHome): ?>
									<div class="col-lg-4 text-center">
										<img class="img-content"src="<?php echo base_url();?>media/<?php echo $getMovieHome->contentimages; ?>" alt="<?php echo $getMovieHome->contenttitle; ?>" title="<?php echo $getMovieHome->contenttitle; ?>">
										<h4><?php echo $getMovieHome->contenttitle; ?></h4>
										<a href="<?php echo base_url(); ?>movie/<?php echo $getMovieHome->contentslug; ?>/"><button class="btn-danger">Read More</button></a>
									</div>
									<?php endforeach; ?>
								</div>
							</div>

							<div class="separator"></div>

						</div>

					</div>
				</div>
				<div class="col-md-3 animated wow fadeInRight">
					<div class="panel">
						<div class="head-panel">
							<h4>MUSIC TOP <span><a href="<?php echo base_url(); ?>music">>> VIEW ALL</a></span></h4>
						</div>
						<div class="body-panel">
							<div class="block-ads">
								<img class="img-content"src="<?php echo base_url();?>assets/img/media/musictop.jpg" alt="" title="">	
							</div>
							<ol class="list-music">
								<li>REAL LOVE<br><span>CLEAN BANDIT</span></li>
								<li>SEE YOU AGAIN<br><span>WIZ KHALIFA FEAT. CHARLIE PUTH</span></li>
								<li>BE MY FOREVER<br><span>CHRISTINA PERRY FEAT. ED SHEERAN</span></li>
								<li>I REALLY LIKE YOU<br><span>CARLI RAE JAPSON</span></li>
								<li>ONE LAST TIME<br><span>ARIANA GRANDE</span></li>
								<li>JEALOUS<br><span>NICK JONAS</span></li>
								<li>FREAKS<br><span>TINNY TRUMPET & SAVAGE</span></li>
								<li>UP<br><span>DEMI LOVATO</span></li>
								<li>STYLE<br><span>TAYLOR SWIFT</span></li>
								<li>I WANT YOU TO KNOW<br><span>ZEDD FEAT SELENA GOMEZ</span></li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="container-separator"></section>
	
	<section class="about" id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-9 animated wow fadeInLeft">
					<div class="panel">
						<div class="head-panel">
							<h4>PROGRAM <span><a href="<?php echo base_url(); ?>program/">>> VIEW ALL</a></span></h4>
						</div>
						<div class="body-panel">
							<?php foreach($getProgramHomeLatest as $getProgramHomeLatest): ?>
							<div class="img-first-content">
								<img src="<?php echo base_url();?>media/<?php echo $getProgramHomeLatest->contentimages; ?>" alt="<?php echo $getProgramHomeLatest->contenttitle; ?>" title="<?php echo $getProgramHomeLatest->contenttitle; ?>">
							</div>
							<div class="text-first-content">
								<h3><?php echo $getProgramHomeLatest->contenttitle; ?></h3>
								<p><?php echo $getProgramHomeLatest->contentheader; ?></p>
								<br>
								<a href="<?php echo base_url(); ?>program/<?php echo $getProgramHomeLatest->contentslug; ?>/"><button class="btn-danger">Read More</button></a>
							</div>
							
							<div class="separator"></div>

							<?php endforeach; ?>

							<div class="list-news">
								<div class="row">
									<?php foreach($getProgramHome as $getProgramHome): ?>
									<div class="col-lg-4 text-center">
										<img class="img-content"src="<?php echo base_url();?>media/<?php echo $getProgramHome->contentimages; ?>" alt="<?php echo $getProgramHome->contenttitle; ?>" title="<?php echo $getProgramHome->contenttitle; ?>">
										<h4><?php echo $getProgramHome->contenttitle; ?></h4>
										<a href="<?php echo base_url(); ?>program/<?php echo $getProgramHome->contentslug; ?>/"><button class="btn-danger">Read More</button></a>
									</div>
									<?php endforeach; ?>
								</div>
							</div>

							<div class="separator"></div>

						</div>

					</div>
				</div>
				<div class="col-md-3 animated wow fadeInRight">
					<div class="panel">
						<div class="head-panel">
							<h4>CONNECT</h4>
						</div>
						<div class="body-panel">
							<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=702663513119101";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>
							<div class="fb-page" data-href="https://www.facebook.com/pages/Radio-LaFemme-88FM/210024759035216" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pages/Radio-LaFemme-88FM/210024759035216"><a href="https://www.facebook.com/pages/Radio-LaFemme-88FM/210024759035216">Radio LaFemme 88FM</a></blockquote></div></div>						
							<br><br>
							<a class="twitter-timeline" href="https://twitter.com/NewLaFemmeFM" data-widget-id="628601438348820481">Tweets by @NewLaFemmeFM</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
<?php $this->load->view('vfooter'); ?>