<?php $this->load->view('vheader'); ?>

<section class="about">
		<div class="container">
			<div class="row">
				<div class="col-md-9 animated wow fadeInLeft">
					<div class="panel">
						<div class="head-panel">
							<h4>MOVIE</h4>
						</div>
						<div class="body-panel">
							<?php foreach($getMovie as $getMovie): ?>
							<div class="row">
								<div class="col-lg-6">
								<div class="img-content">
									<img class="img-thumbnail" src="<?php echo base_url();?>media/<?php echo $getMovie->contentimages; ?>" alt="<?php echo $getMovie->contenttitle; ?>" title="<?php echo $getMovie->contenttitle; ?>">
								</div>
								</div>
								<div class="col-lg-6">
								<div class="text-first-content">
									<h3><?php echo $getMovie->contenttitle; ?></h3>
									<p class="text-date"><?php echo $getMovie->contentcreatedate; ?></p>
									<p><?php echo $getMovie->contentheader; ?></p>
									<a href="<?php echo base_url();?>movie/<?php echo $getMovie->contentslug; ?>/"><button class="btn-danger">Read More</button></a>
								</div>
								</div>
							</div>
							
							<div class="row"><div class="col-lg-12 text-center"><div class="separator"></div></div></div>
							<?php endforeach; ?>
						</div>

					</div>
				</div>
				<div class="col-md-3 animated wow fadeInRight">
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner-300x250.jpg" alt="" title="">	
					</div>	
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner2-300x250.jpg" alt="" title="">	
					</div>	
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner-300x250.jpg" alt="" title="">	
					</div>			
					<div class="block-ads">
						<img class="img-content"src="<?php echo base_url();?>assets/img/banner/banner2-300x250.jpg" alt="" title="">	
					</div>	
				</div>
			</div>
		</div>

	</section>

<?php $this->load->view('vfooter'); ?>