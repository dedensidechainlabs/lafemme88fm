	<!--  Footer Section  -->
	<footer>
		<div class="row" style="margin-left:0px; margin-right:0px;">
			<div class="col-lg-2 col-lg-offset-1">
				<ul class="footer-menu">
					<li><a href="<?php echo base_url();?>about/">ABOUT US</a></li>
					<li><a href="<?php echo base_url();?>advertise/">ADVERTISE WITH US</a></li>
					<li><a href="<?php echo base_url();?>career/">CAREER</a></li>
					<li><a href="<?php echo base_url();?>faq/">FAQ</a></li>
					<li><a href="<?php echo base_url();?>website/">ABOUT THE WEBSITE</a></li>
					<li><a href="<?php echo base_url();?>contact/">CONTACT US</a></li>
				</ul>
			</div>
			<div class="col-lg-2">
				<ul class="footer-menu">
					<li>OUR STATION</li>
					<li>MEDAN 88 FM</li>
					<li>&nbsp;</li>
					<li>OUR GROUP</li>
					<li>
						<a href="http://rightkliq.com/" target="_blank"><img width="40" src="<?php echo base_url(); ?>assets/img/logo-rightkliq.png" alt="" title=""></a> &nbsp; &nbsp; 
						<a href="http://foodtruckindonesa.com/" target="_blank"><img width="80" src="<?php echo base_url(); ?>assets/img/logo-foodtruckindonesia.png" alt="" title=""></a>
					</li>
				</ul>
			</div>
			<div class="col-lg-2">
				<ul class="footer-menu">
					<li><a href="https://www.facebook.com/pages/Radio-LaFemme-88FM/210024759035216" target="_blank"><img src="<?php echo base_url();?>assets/img/facebook.png" width="30"> FACEBOOK</a> </li>
					<li><a href="https://twitter.com/newlafemmefm" target="_blank"><img src="<?php echo base_url();?>assets/img/twitter.png" width="30"> TWITTER</a> </li>
					<li><a href="#"><a href="#" target="_blank"><img src="<?php echo base_url();?>assets/img/instagram.png" width="30"> INSTAGRAM</a></li>
					<li><a href="https://www.youtube.com/channel/UCIuZYn11OSGXzQBXTTNEHrA" target="_blank"><img src="<?php echo base_url();?>assets/img/youtube.png" width="30"> YOUTUBE</a> </li>
					
				</ul>
			</div>
			<div class="col-lg-3">
				<ul class="footer-menu">
					<li>SUBSCRIBE OUR NEWSLETTER</li>
					<li>
						<form>
						  <div class="form-group">
						    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
						  </div>
						  <button type="submit" class="btn btn-default">Submit</button>
						</form>
					</li>
					<li><p class="copyright animated wow fadeIn" data-wow-duration="2s">© 2015 Powered by <a href="http://sidechainlabs.com" target="_blank"><strong>Sidechain Labs</strong></a>. All Rights Reserved</p></li>
				</ul>
			</div>
		</div>
	</footer>
	<!--  End Footer Section  -->


</body>
</html>