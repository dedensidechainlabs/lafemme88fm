<?php
class Mdata extends CI_Model
{
	
    function getUsers() 
    {
        $this->db->from('users');
        $this->db->order_by('created_at','DESC');
        $query = $this->db->get();
        return $query->result();
        
    }

    function getContent($category, $limit, $offset)
    {
        $this->db->from('tcontent');
        $this->db->where('categoryname',$category);
        $this->db->order_by('contentcreatedate','DESC');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result();
    }

    function getContentDetail($category,$page)
    {
        $this->db->from('tcontent');
        $this->db->where('categoryname', $category);
        $this->db->where('contentslug', $page);
        $query = $this->db->get();
        return $query->result();
    }

}

/*

CREATE TABLE tuser (
    userid INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255),
    userpassword VARCHAR(255),
    userlevel INT,
    PRIMARY KEY (userid)
);

CREATE TABLE tcategory (
    categoryid INT NOT NULL AUTO_INCREMENT,
    categoryname VARCHAR(255),
    categorystatus INT,
    PRIMARY KEY (categoryid)
);

CREATE TABLE tcontent (
    contentid INT NOT NULL AUTO_INCREMENT,
    contenttitle VARCHAR(255),
    contentheader TEXT,
    contentfull TEXT,
    contentimages VARCHAR(255),
    contentpublish INT,
    contentcreatedate DATETIME,
    contentupdatedate DATETIME,
    categoryname VARCHAR(255),
    PRIMARY KEY (contentid)
);

INSERT INTO tuser (username,userpassword,userlevel) VALUES ('lafemme-admin','12345','1');

INSERT INTO tcategory (categoryname,categorystatus) VALUES ('music','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('movie','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('program','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('news','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('dj','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('event','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('about-us','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('advertise-with-us','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('career','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('faq','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('about-the-website','1');
INSERT INTO tcategory (categoryname,categorystatus) VALUES ('contact-us','1');

ALTER TABLE tcategory ADD COLUMN metatitle VARCHAR(255);
ALTER TABLE tcategory ADD COLUMN metadescription VARCHAR(255);

UPDATE tcategory SET metatitle='Music - La Femme 88 FM', metadescription='' WHERE categoryname='music';
UPDATE tcategory SET metatitle='Movie - La Femme 88 FM', metadescription='' WHERE categoryname='movie';
UPDATE tcategory SET metatitle='Program - La Femme 88 FM', metadescription='' WHERE categoryname='program';
UPDATE tcategory SET metatitle='News - La Femme 88 FM', metadescription='' WHERE categoryname='news';
UPDATE tcategory SET metatitle='DJ - La Femme 88 FM', metadescription='' WHERE categoryname='dj';
UPDATE tcategory SET metatitle='Event - La Femme 88 FM', metadescription='' WHERE categoryname='event';
UPDATE tcategory SET metatitle='About Us - La Femme 88 FM', metadescription='' WHERE categoryname='about-us';
UPDATE tcategory SET metatitle='Advertise With Us - La Femme 88 FM', metadescription='' WHERE categoryname='advertise-with-us';
UPDATE tcategory SET metatitle='Career - La Femme 88 FM', metadescription='' WHERE categoryname='career';
UPDATE tcategory SET metatitle='FAQ - La Femme 88 FM', metadescription='' WHERE categoryname='faq';
UPDATE tcategory SET metatitle='About the Website - La Femme 88 FM', metadescription='' WHERE categoryname='about-the-website';
UPDATE tcategory SET metatitle='Contact Us - La Femme 88 FM', metadescription='' WHERE categoryname='contact-us';

ALTER TABLE tcontent ADD COLUMN contentslug VARCHAR(255);

*/
?>

