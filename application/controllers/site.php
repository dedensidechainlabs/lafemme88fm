<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('mdata');
		$this->load->helper(array('form', 'url','file'));
		date_default_timezone_set('Asia/Jakarta');
	}	

	public function index()
	{
		$data['title'] = "Lafemme 88 FM" ;
		$data['desription'] = "Lafemme 88 FM - The Meaning of You" ;

		$data['getNewsHomeLatest'] = $this->mdata->getContent('news','1','0');
		$data['getNewsHome'] = $this->mdata->getContent('news','3','1');
		$data['getEventHome'] = $this->mdata->getContent('event','3','0');
		$data['getMovieHomeLatest'] = $this->mdata->getContent('movie','1','0');
		$data['getMovieHome'] = $this->mdata->getContent('movie','3','1');
		$data['getProgramHomeLatest'] = $this->mdata->getContent('program','2','0');
		$data['getProgramHome'] = $this->mdata->getContent('program','3','2');

		$this->load->view('vindex',$data);
	}
	
	function music($page="")
	{
		if($page==NULL){
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('vmusic',$data);
		}else{
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('vmusicdetail',$data);
		}
	}

	function movie($page="")
	{
		if($page==NULL){
			$queryMeta = $this->db->get_where('tcategory', array('categoryname' => 'movie'))->row();
			$data['title'] = $queryMeta->metatitle;
			$data['description'] = $queryMeta->metadescription;
			
			$data['getMovie'] = $this->mdata->getContent('movie',100,0); 

			$this->load->view('vmovie',$data);
		}else{
			
			$queryMeta = $this->db->get_where('tcategory', array('categoryname' => 'movie'))->row();
			$data['title'] = $queryMeta->metatitle;
			$data['description'] = $queryMeta->metadescription;

			$data['getMovieDetail'] = $this->mdata->getContentDetail('movie', $page); 
			
			$this->load->view('vmoviedetail',$data);
		}	
	}

	function program($page="")
	{
		if($page==NULL){
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('vprogram',$data);
		}else{
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('vprogramdetail',$data);
		}
	}

	function news($page="")
	{
		if($page==NULL){
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('vnews',$data);
		}else{
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('vnewsdetail',$data);
		}
	}

	function djs($page="")
	{
		if($page==NULL){
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('vdjs',$data);
		}else{
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('vdjsdetail',$data);
		}
	}

	function event($page="")
	{
		if($page==NULL){
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('vevent',$data);
		}else{
			$data['title'] = "Lafemme88fm | The Meaning of You";
			$this->load->view('veventdetail',$data);
		}
	}

	function about()
	{
		$data['title'] = "Lafemme88fm | The Meaning of You";
		$this->load->view('vabout',$data);
	}

	function advertise()
	{
		$data['title'] = "Lafemme88fm | The Meaning of You";
		$this->load->view('vadvertise',$data);
	}

	function career()
	{
		$data['title'] = "Lafemme88fm | The Meaning of You";
		$this->load->view('vcareer',$data);
	}

	function faq()
	{
		$data['title'] = "Lafemme88fm | The Meaning of You";
		$this->load->view('vfaq',$data);
	}

	function website()
	{
		$data['title'] = "Lafemme88fm | The Meaning of You";
		$this->load->view('vwebsite',$data);
	}

	function contact()
	{
		$data['title'] = "Lafemme88fm | The Meaning of You";
		$this->load->view('vcontact',$data);
	}
}