<?php
	require_once "extras/lib.php";
	$radio = new Radio("103.28.148.18:8000/lafemme"); 

	$display_array = array("Stream Title", "Stream Genre", "Stream URL", "Current Song","Listener Peak");

	$data_array = $radio->getServerInfo($display_array);

	$data_arrays = $radio->getHistoryTable();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lafemme 88 FM - Online Radio</title>
  <link rel="stylesheet" href="css/normalize.css">
	<link type="text/css" href="css/style.css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
	<script>
	$(document).ready(function(){
      $("#radiop").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            mp3: "http://103.28.148.18:8000/lafemme"
          });
        },
        swfPath: "/js",
        supplied: "mp3,oga"
      });
    });
	</script>
</head>
<body>
<div id="radiop" class="jp-jplayer"></div>
<div id="jp_container_1" class="jp-audio">
<img src="images/logo.png" class="logos" alt="Lafemme 88 fm"/>
  <div class="jp-type-single">
    <div class="jp-title">
      <ul>
        <li><?php  echo $data_array[3];?></li>
      </ul>
    </div>
    <div class="jp-gui jp-interface">

        <ul class="jp-controls">
          <li><a href="javascript:;" class="jp-play" tabindex="1">&#61515;</a></li>
          <li><a href="javascript:;" class="jp-pause" tabindex="1">&#61516;</a></li>
          <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">&#61480;</a></li>
          <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">&#61478;</a></li>
        </ul>
        <div class="jp-progress">
          <div class="jp-seek-bar">
            <div class="jp-play-bar"></div>
          </div>
        </div>
        <div class="jp-time-holder">
          <div class="jp-current-time"></div>
        </div>
        <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div>

    <div class="jp-no-solution">
      <span>Diperlukan Update</span>
     Untuk menggunakan pemutar media ini anda harus mengupgrade browser anda atau mengunduh Adobe Flash Player terbaru.
    </div>
  </div>

  <div class="jp-gui jp-interface title">
	 Lagu terakhir diputar
  </div>
  <div class="jp-gui jp-interface title">
  		<?php echo $data_arrays;?>
  </div>
</div>
</body>
</html>